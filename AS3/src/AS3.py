import cv2
import numpy as np
import math
import sys
from time import time
from lib import * 


def nothing(x):
    pass
def load_img(argv):
    if len(argv) == 3:
        file1 = argv[1]
        file2 = argv[2]
        img1 = cv2.imread(file1)
        img2 = cv2.imread(file2)
    else:
        img1 = cv2.imread("../data/img1.jpg")
        img2 = cv2.imread("../data/img2.jpg")
    img1_gray = np.float32(cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY))
    img2_gray = np.float32(cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY))
    return img1, img1_gray, img2, img2_gray
def main():
    list = []
    print "reading files..."
    img1, img1_gray, img2, img2_gray = load_img(list)
    winName1 = "image1"
    winName2 = "image2"
    img1c = img1.copy()
    img2c = img2.copy()
    print "finding corners for first image..."
    flist1 = harris_detection_and_localize(img1_gray, 10, 10, 5, 5)
    print "finding corners for second image..."
    flist2 = harris_detection_and_localize(img2_gray, 10, 10, 5, 5)
    corner1 = np.int0(flist1)
    corner2 = np.int0(flist2)
    print "matching corners..."
    corner_match(img1c, img2c, corner1, corner2)
    cv2.imshow(winName1, img1c)
    cv2.imshow(winName2, img2c)
    cv2.createTrackbar('Variance of Gaussian(x0.5)', winName1, 5, 30, nothing)
    cv2.createTrackbar('Size of Neighborhood', winName1, 10, 20, nothing)
    cv2.createTrackbar('Weight of trace(x0.01)', winName1, 10, 15, nothing)
    cv2.createTrackbar('Threshold for Harris_score(%)', winName1, 5, 50, nothing)
    
    # parameters for text
    height = img1c.shape[0]
    weight = img1c.shape[1]
    interval = height/10
    font = cv2.FONT_HERSHEY_TRIPLEX
    color = (50, 255, 50)
            
    while(True):
        print "wait for pressing key to operate next..."
        key = cv2.waitKey()
        #Help key
        if key == ord('h'):            
            description = []
            list.append('cmd arguments:')
            list.append('--filename: the path of two files which will be read in')
            list.append('            Otherwise read in default image')
            list.append('Interface keys:')
            list.append('h: show help discription')
            list.append('ESC: shut down program')
            list.append('Notice: ')
            list.append('You should wait several seconds for processing')
            for i in range(0, len(list)):
                cv2.putText(img1c, list[i], (0, (i+1)*interval), font, 0.4, color)
            cv2.imshow(winName1, img1c)
        #Exit key
        elif key == 27 :
            cv2.destroyAllWindows()
            break
        #Otherwise processing image.
        else:
            img1c = img1.copy()
            img2c = img2.copy()
            sigma_scale = cv2.getTrackbarPos('Variance of Gaussian(x0.5)',winName1)
            n_size = cv2.getTrackbarPos('Size of Neighborhood',winName1)
            k_scale = cv2.getTrackbarPos('Weight of trace(x0.01)',winName1)
            threshold_scale = cv2.getTrackbarPos('Threshold for Harris_score(%)',winName1)
            print "finding corners for first image..."   
            flist1 = harris_detection_and_localize(img1_gray, k_scale, n_size, sigma_scale, threshold_scale)
            print "finding corners for second image..."
            flist2 = harris_detection_and_localize(img2_gray, k_scale, n_size, sigma_scale, threshold_scale)
            corner1 = np.int0(flist1)
            corner2 = np.int0(flist2)
            print "matching corners..."
            corner_match(img1c, img2c, corner1, corner2)
            cv2.imshow(winName1, img1c)
            cv2.imshow(winName2, img2c)

    
if __name__ == '__main__':
    main()