import cv2
import numpy as np
from time import time

def corner_match(img1_origin, img2_origin, flist1, flist2):
    img1 = img1_origin.copy()
    img2 = img2_origin.copy()
    
    corner1 = np.int0(flist1)
    corner2 = np.int0(flist2)

    for i in range(0, len(corner1)):
        cv2.rectangle(img1, (corner1[i,0] - 10, corner1[i,1] - 10), (corner1[i,0] + 10, corner1[i,1] + 10), [0,0,125])
    for j in range(0, len(corner2)):
        cv2.rectangle(img2, (corner2[j,0] - 10, corner2[j,1] - 10), (corner2[j,0] + 10, corner2[j,1] + 10), [0,0,125])

    index = 1
    list1 = range(0, len(corner1))
    list2 = range(0, len(corner2))
    font = cv2.FONT_HERSHEY_TRIPLEX
    color = (50, 255, 50)

    for i in list1:
        min_dis = 0
        min_index = -1
        for j in list2:
            dis = (corner1[i][0] - corner2[j][0])**2 + (corner1[i][1] - corner2[j][1])**2
            if min_index == -1:
                min_dis = dis
                min_index = j
            else:
                if dis < min_dis:
                    min_dis = dis
                    min_index = j
        cv2.putText(img1, str(index),(corner1[i,0],corner1[i,1]), font, 0.5, color)
        cv2.putText(img2, str(index),(corner2[min_index,0],corner2[min_index,1]), font, 0.5, color)
        index = index + 1
    return img1, img2

img1 = cv2.imread("../data/img1.jpg")
img2 = cv2.imread("../data/img2.jpg")
img1_gray = np.float32(cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY))
img2_gray = np.float32(cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY))

start = time()
n_size = 3
opencv1 = cv2.cornerHarris(img1_gray, n_size, 5, 0.1)
print "Run Time: %.2f sec" % (time() - start)
start = time()
opencv2 = cv2.cornerHarris(img2_gray, n_size, 5, 0.1)
print "Run Time: %.2f sec" % (time() - start)

ret, dst = cv2.threshold(opencv1,0.1*opencv1.max(),255,0)
dst = np.uint8(dst)
# find centroids
ret, labels, stats, centroids = cv2.connectedComponentsWithStats(dst)
# define the criteria to stop and refine the corners
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
corners1 = cv2.cornerSubPix(img1_gray,np.float32(centroids),(5,5),(-1,-1),criteria)

ret, dst = cv2.threshold(opencv2,0.1*opencv2.max(),255,0)
dst = np.uint8(dst)
# find centroids
ret, labels, stats, centroids = cv2.connectedComponentsWithStats(dst)
# define the criteria to stop and refine the corners
criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
corners2 = cv2.cornerSubPix(img2_gray,np.float32(centroids),(5,5),(-1,-1),criteria)

show3, show4 = corner_match(img1, img2, corners1, corners2)
cv2.imshow("image1", show3)
cv2.imshow("image2", show4)
cv2.waitKey()
cv2.destroyWindow("image1")
cv2.destroyWindow("image2")