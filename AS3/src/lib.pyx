import cv2
import numpy as np
import math
from time import time
cimport numpy as np
cimport cython

def harris_detection_and_localize(np.ndarray[np.float32_t, ndim=2] img, int k_scale, int n_size, int sigma_scale, int threshold_scale):
    cdef int x, y, x_size, y_size, side1, side2, xleft, xright, ytop, ybottom, window_x, window_y
    cdef np.ndarray[np.float32_t, ndim=2] sobelx, sobely, Ixx, Iyy, Ixy, harris_score
    cdef np.ndarray[np.float32_t, ndim=2] M
    
    sigma = sigma_scale * 0.5
    smooth_size = int(round(5 * sigma))
    if smooth_size % 2 == 0:
        smooth_size += 1
    img_smooth = cv2.GaussianBlur(img, (smooth_size, smooth_size), sigma)
    
    sobelx = cv2.Sobel(img, cv2.CV_32F, 1, 0, ksize = 5)
    sobely = cv2.Sobel(img, cv2.CV_32F, 0, 1, ksize = 5)
    
    Ixx = np.square(sobelx)
    Iyy = np.square(sobely)
    Ixy = np.multiply(sobelx, sobely)
    
    x_size = img.shape[1]
    y_size = img.shape[0]
    side1 = -(n_size - 1)/2
    side2 = n_size / 2
    harris_score = np.zeros((img.shape[0], img.shape[1]), np.float32)

    for x in xrange(x_size):
        for y in xrange(y_size):
            # compute the window range       
            xleft = max(0, x + side1)
            xright = min(x_size, x + side2 + 1)
            ytop = max(0, y + side1)
            ybottom = min(y_size, y + side2 + 1)
            
            M = np.zeros((2,2), np.float32)
            for window_x in range(xleft, xright):
                for window_y in range(ytop, ybottom):
                    M[0,0] += Ixx[window_y, window_x]
                    M[0,1] += Ixy[window_y, window_x]
                    M[1,1] += Iyy[window_y, window_x]
            M[1,0] = M[0,1]
            harris_score[y,x] = np.linalg.det(M) - k_scale * 0.01 * (np.trace(M) ** 2)
            
    harris_max = harris_score.max()
    threshold_val = 0.01 * threshold_scale * harris_max
    
    # localization for corner
    featurelist = []
    for x in xrange(x_size):
        for y in xrange(y_size):
            if harris_score[y,x] > threshold_val:
                # compute the window range                 
                xleft = max(0, x + side1)
                xright = min(x_size, x + side2 + 1)
                ytop = max(0, y + side1)
                ybottom = min(y_size, y + side2 + 1)
                
                # if there exists a point has greater harris score, skip this point
                whether_max = True
                for window_x in range(xleft, xright):
                    if whether_max == False:
                        break
                    for window_y in range(ytop, ybottom):
                        if harris_score[y,x] < harris_score[window_y,window_x]:
                            whether_max = False
                            break
                if whether_max == False:
                    continue
                            
                C = np.zeros((2,2), np.float32)
                V = np.zeros((2,1), np.float32)
                for window_x in range(xleft, xright):
                    for window_y in range(ytop, ybottom):
                        g = np.array([[sobelx[window_y][window_x]],[sobely[window_y][window_x]]], np.float32)
                        p = np.array([[x],[y]], np.float32)
                        Ci = g.dot(g.T)
                        C += Ci
                        V += Ci.dot(p)
                if np.linalg.det(C) != 0:
                    localization = np.linalg.inv(C).dot(V)
                    xl = int(localization[0][0])
                    yl = int(localization[1][0])
                else:
                    xl = x
                    yl = y      
                featurelist.append([xl, yl, img[yl][xl]])
    
    return featurelist



def corner_match(np.ndarray img1, np.ndarray img2, np.ndarray corner1, np.ndarray corner2):
    cdef int i, j, len1, len2

    len1 = len(corner1)
    len2 = len(corner2)

    for i in xrange(len1):
        cv2.rectangle(img1, (corner1[i,0] - 10, corner1[i,1] - 10), (corner1[i,0] + 10, corner1[i,1] + 10), [0,0,125])
    for j in xrange(len2):
        cv2.rectangle(img2, (corner2[j,0] - 10, corner2[j,1] - 10), (corner2[j,0] + 10, corner2[j,1] + 10), [0,0,125])

    index = 1
    font = cv2.FONT_HERSHEY_TRIPLEX
    color = (50, 255, 50)

    for i in xrange(len1):
        min_dis = 0
        min_index = -1
        for j in xrange(len2):
            dis = (corner1[i][0] - corner2[j][0])**2 + (corner1[i][1] - corner2[j][1])**2 + ((corner1[i][2] - corner2[j][2]))**2
            if min_index == -1:
                min_dis = dis
                min_index = j
            else:
                if dis < min_dis:
                    min_dis = dis
                    min_index = j
        cv2.putText(img1, str(index),(corner1[i,0],corner1[i,1]), font, 0.5, color)
        cv2.putText(img2, str(index),(corner2[min_index,0],corner2[min_index,1]), font, 0.5, color)
        index = index + 1