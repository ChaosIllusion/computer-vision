import cv2
import numpy as np
from time import time

def localization(img, harris_score, n_size, threshold_scale):
    x_size = img.shape[1]
    y_size = img.shape[0]
    side1 = (n_size - 1)/2
    side2 = n_size / 2
    sobelx = cv2.Sobel(img, cv2.CV_32F, 1, 0, ksize = 5)
    sobely = cv2.Sobel(img, cv2.CV_32F, 0, 1, ksize = 5)
    # localization for corner
    featurelist = []
    harris_max = harris_score.max()
    threshold_val = 0.01 * threshold_scale * harris_max
    for x in xrange(x_size):
        for y in xrange(y_size):
            if harris_score[y,x] > threshold_val:
                # compute the window range                 
                xleft = max(0, x - side1)
                xright = min(x_size, x + side2 + 1)
                ytop = max(0, y - side1)
                ybottom = min(y_size, y + side2 + 1)
                
                # if there exists a point has greater harris score, skip this point
                whether_max = True
                for window_x in range(xleft, xright):
                    if whether_max == False:
                        break
                    for window_y in range(ytop, ybottom):
                        if harris_score[y,x] < harris_score[window_y,window_x]:
                            whether_max = False
                            break
                if whether_max == False:
                    continue
                            
                C = np.zeros((2,2), np.float32)
                V = np.zeros((2,1), np.float32)
                for window_x in range(xleft, xright):
                    for window_y in range(ytop, ybottom):
                        g = np.array([[sobelx[window_y][window_x]],[sobely[window_y][window_x]]], np.float32)
                        p = np.array([[x],[y]], np.float32)
                        Ci = g.dot(g.T)
                        C += Ci
                        V += Ci.dot(p)
                localization = np.linalg.inv(C).dot(V)
                xl = int(localization[0][0])
                yl = int(localization[1][0])         
                featurelist.append([xl, yl, img[yl][xl]])
                        
    return featurelist

def corner_match(img1_origin, img2_origin, flist1, flist2):
    img1 = img1_origin.copy()
    img2 = img2_origin.copy()
    
    corner1 = np.int0(flist1)
    corner2 = np.int0(flist2)

    for i in range(0, len(corner1)):
        cv2.rectangle(img1, (corner1[i,0] - 10, corner1[i,1] - 10), (corner1[i,0] + 10, corner1[i,1] + 10), [0,0,125])
    for j in range(0, len(corner2)):
        cv2.rectangle(img2, (corner2[j,0] - 10, corner2[j,1] - 10), (corner2[j,0] + 10, corner2[j,1] + 10), [0,0,125])

    index = 1
    list1 = range(0, len(corner1))
    list2 = range(0, len(corner2))
    font = cv2.FONT_HERSHEY_TRIPLEX
    color = (50, 255, 50)

    for i in list1:
        min_dis = 0
        min_index = -1
        for j in list2:
            dis = (corner1[i][0] - corner2[j][0])**2 + (corner1[i][1] - corner2[j][1])**2 + ((corner1[i][2] - corner2[j][2]))**2
            if min_index == -1:
                min_dis = dis
                min_index = j
            else:
                if dis < min_dis:
                    min_dis = dis
                    min_index = j
        cv2.putText(img1, str(index),(corner1[i,0],corner1[i,1]), font, 0.5, color)
        cv2.putText(img2, str(index),(corner2[min_index,0],corner2[min_index,1]), font, 0.5, color)
        index = index + 1
    return img1, img2

img1 = cv2.imread("../data/img1.jpg")
img2 = cv2.imread("../data/img2.jpg")
img1_gray = np.float32(cv2.cvtColor(img1, cv2.COLOR_BGR2GRAY))
img2_gray = np.float32(cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY))

start = time()
n_size = 3
opencv1 = cv2.cornerHarris(img1_gray, n_size, 5, 0.1)
flist3 = localization(img1_gray, opencv1, n_size, 10)
print "Run Time: %.2f sec" % (time() - start)
start = time()
opencv2 = cv2.cornerHarris(img2_gray, n_size, 5, 0.1)
flist4 = localization(img2_gray, opencv2, n_size, 10)
print "Run Time: %.2f sec" % (time() - start)

show3, show4 = corner_match(img1, img2, flist3, flist4)
cv2.imshow("image1", show3)
cv2.imshow("image2", show4)
cv2.waitKey()
cv2.destroyWindow("image1")
cv2.destroyWindow("image2")