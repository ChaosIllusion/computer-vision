
# coding: utf-8

# In[1]:

import numpy as np
import math
from sympy import *


# # A

# In[2]:

A=np.array([[1],[2],[3]])
B=np.array([[4],[5],[6]])
C=np.array([[-1],[1],[3]])


# In[3]:

print("A.")
print("1. ",2*A-B)
det = np.linalg.norm(A)
print("2. ", det, " angle: ", math.acos(1/det))
print("3. ", A/det)
l = np.array(A/det)
print("4. ", l.reshape(3,order='C'))
print("5. ", A.T.dot(B), " ", B.T.dot(A))
cos_value = A.T.dot(B)/(np.linalg.norm(A) * np.linalg.norm(B))
print("6. ", math.cos(cos_value))
#ans = [x, 1, 0]
value = np.array([-1 * A[1]])
V = np.array([[A[0]]])
ans = np.linalg.solve(V,value)
ans = np.array([np.hstack((ans[0], np.array([1,0])))])
print("7 .", ans.T)


# In[4]:

v1 = np.cross(A.T,B.T)
v2 = np.cross(B.T, A.T)
print("8 .", v1.T, "\nand\n", v2.T)
print("9 .", v1.T)
#ans = [x, y, 1]
value = np.array([-1 * C[0], -1 * C[1]])
V = np.hstack((A[0:2], B[0:2]))
ans = np.linalg.solve(V,value)
#and we find -3*3 + 6 + 3 = 0
print("10 . the linear dependency ", np.hstack((ans.T[0], np.array([1]))))
A1 = np.matrix(A)
B1 = np.matrix(B)
print("11. ", A1.T*B1, " and \n" , A1*B1.T)


# # B

# In[5]:

A = np.matrix([[1,2,3],[4,-2,3],[0,5,-1]])
B = np.matrix([[1,2,1],[2,1,-4],[3,-2,1]])
C = np.matrix([[1,2,3],[4,5,6],[-1,1,3]])


# In[6]:

print("B.")
print("1.\n", 2*A - B)
print("2.\n", A*B, "\n and \n", B*A )
print("3.\n", (A*B).T, "\n and \n", B.T*A.T )


# In[7]:

print("4.\n", np.linalg.det(A), " and ", np.linalg.det(C))
# for B
print("5. For B, each row dot multiply ans:", B[0]*B[1].T, " ", B[1]*B[2].T, " ", B[2]*B[0].T)
print("Therefore, the answer is B.")
print("6. ", A.I, "\nand\n", B.I)


# # C

# In[8]:

A = np.matrix([[1,2],[3,2]])
B = np.matrix([[2,-2],[-2,5]])


# In[9]:

print("C.")
value, vector = np.linalg.eig(A)
print("1. value:\n", value, "\nvector:\n", vector)
print("2. ", vector.I*A*vector)
print("3. ", vector[:,0].T.dot(vector[:,1]))
value, vector = np.linalg.eig(B)
print("4. ", vector[:,0].T.dot(vector[:,1]))
print("5. the B is a symmetric real matrix, its eigenvectors are orthogonal." )

