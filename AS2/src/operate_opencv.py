import cv2
import numpy as np
import sys
import math

def toGrayscale(img):
    show = np.ones((img.shape[0], img.shape[1]),np.uint8)
    for i in range(0, img.shape[0], 1):
        for j in range(0, img.shape[1], 1):
            show[i][j] = img[i][j][0] * 0.114 + img[i][j][1] * 0.587+ img[i][j][2] * 0.2989
    return show

def sliderHandler(n):
    src = cur
    if n!=0:
        kernel = np.ones((n,n), np.float32)/(n*n)
        dst = cv2.filter2D(src, -1, kernel)
    else:
        dst = src
    cv2.imshow(winName, dst)

def silderSmooth(n):
    src = cur
    if n > 1:
        left = - ((n-1) / 2)
        right = n + left
        xSmooth = np.ones((src.shape[0], src.shape[1]),np.uint8)
        
        for i in range(0, src.shape[0], 1):
            psum = 0
            for j in range(0-left, src.shape[1]-right+1, 1):
                if(j == 0-left):
                    for y in range(left, right, 1):                           
                        psum = psum + src[i][y+j]
                else:
                    psum = psum + src[i][j+right-1] - src[i][j+left-1]
                xSmooth[i][j] = psum / n
        
        dst = np.ones((src.shape[0], src.shape[1]),np.uint8)
        
        for j in range(0-left, xSmooth.shape[1]-right+1, 1):
            psum = 0
            for i in range(0-left, xSmooth.shape[0]-right+1, 1):
                if(i == 0-left):
                    for x in range(left, right, 1):             
                            psum = psum + xSmooth[x+i][j]
                else:
                    psum = psum + xSmooth[i+right-1][j] - xSmooth[i+left-1][j]
                dst[i][j] = psum / n
    else:
        dst = src
    cv2.imshow(winName, dst)

def silderVector(N):
    K = 15
    show = cv2.cvtColor(cur, cv2.COLOR_BGR2GRAY)
    x = cv2.Sobel(show,cv2.CV_16S,1,0)
    y = cv2.Sobel(show,cv2.CV_16S,0,1)
    for i in range(0, show.shape[0], N):
        for j in range(0, show.shape[1], N):
            pt1 = (j, i)
            x1 = float(x[i][j])
            y1 = float(y[i][j])
            scale = math.sqrt(x1*x1+y1*y1) / K
            if scale != 0:
                pt2 = (j+int(x1/scale), i+int(y1/scale))
            else:
                pt2 = pt1
            cv2.arrowedLine(show, pt1, pt2, (0,0,255), 2)
    cv2.imshow(winName, show)

def silderRotate(R):
    show = cv2.cvtColor(cur, cv2.COLOR_BGR2GRAY)
    rows = show.shape[0]
    cols = show.shape[1]
    M = cv2.getRotationMatrix2D((cols/2, rows/2), R, 1)
    show = cv2.warpAffine(show, M, (cols,rows))
    cv2.imshow(winName, show)
    


if len(sys.argv) == 2:
    filename = sys.argv[1]
    image = cv2.imread(filename)
elif len(sys.argv) < 2:
    cap = cv2.VideoCapture(0)
    retval,image = cap.read()

winName = 'Sample Image'
cv2.imshow(winName, image)
cur = image.copy() #current processed image
show = cur #the displayed image
channel = 0 #the channel of image to be shown
while(True):
    key = cv2.waitKey()

    if key == ord('i'):
        cur = image.copy()
        show = cur
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)

    elif key == ord('w'):
        cv2.imwrite("out.jpg", show)

    elif key == ord('g'):
        show = cv2.cvtColor(cur, cv2.COLOR_BGR2GRAY)
        cur = show
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)

    elif key == ord('G'):
        show = toGrayscale(cur)
        cur = show
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)

    elif key == ord('c'):
        show = np.zeros((cur.shape[0], cur.shape[1], 3),np.uint8)
        for i in range(0, cur.shape[0], 1):
            for j in range(0, cur.shape[1], 1):
                show[i][j][channel] = cur[i][j][channel]
        channel = (channel + 1) % 3
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)

    elif key == ord('s'):
        show = cv2.cvtColor(cur, cv2.COLOR_BGR2GRAY)
        cur = show
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)
        size = max(cur.shape[0], cur.shape[1])
        cv2.createTrackbar('s', winName, 0, size/10, sliderHandler)
    
    elif key == ord('S'):
        show = toGrayscale(cur)
        cur = show
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)
        size = max(cur.shape[0], cur.shape[1])
        cv2.createTrackbar('S', winName, 0, size/10, silderSmooth)

    elif key == ord('d'):
        size = (cur.shape[1]/2, cur.shape[0]/2)
        show = cv2.resize(cur, size, interpolation=cv2.INTER_NEAREST)
        cur = show
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)
        
    elif key == ord('D'):
        show = cv2.pyrDown(cur)
        cur = show
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)
        
    elif key == ord('x'):
        show = cv2.cvtColor(cur, cv2.COLOR_BGR2GRAY)
        cur = cv2.Sobel(show,-1,1,0)
        show = cur
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)
        
    elif key == ord('y'):
        show = cv2.cvtColor(cur, cv2.COLOR_BGR2GRAY)
        cur = cv2.Sobel(show,-1,0,1)
        show = cur
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)
        
    elif key == ord('m'):
        show = cv2.cvtColor(cur, cv2.COLOR_BGR2GRAY)
        cur = cv2.Sobel(show,-1,1,1)
        show = cur
        cv2.destroyWindow(winName)
        cv2.imshow(winName, show)
        
    elif key == ord('p'):
        cv2.destroyWindow(winName)
        silderVector(30)
        size = max(cur.shape[0], cur.shape[1])
        cv2.createTrackbar('p', winName, 30, size/5, silderVector)
        
    elif key == ord('r'):
        cv2.destroyWindow(winName)
        silderRotate(0)
        size = max(cur.shape[0], cur.shape[1])
        cv2.createTrackbar('p', winName, 0, 360, silderRotate)
        
    elif key == ord('h'):
        height = cur.shape[0]
        interval = height/20
        font = cv2.FONT_HERSHEY_TRIPLEX
        color = (50, 150, 50)
        list = []
        list.append('cmd arguments: --filename: show the path of file which will be read in')
        list.append('Interface keys:')
        list.append('i: reload original img')
        list.append('w: save img to out.jgp')
        list.append('g: convert to grayscale using openCV funtion')
        list.append('G: convert to grayscale using my funtion')
        list.append('c: cycle through the color channel')
        list.append('s: convert to grayscale and smooth by openCV with trackbar controling filter size')
        list.append('S: convert to grayscale and smooth by my funcions with trackbar controling filter size')
        list.append('d: downsample the iamge by 2 without smoothing')
        list.append('D: downsample the iamge by 2 with smoothing')
        list.append('x: convert to grayscale and convolute with x derivative filter')
        list.append('y: convert to grayscale and convolute with y derivative filter')
        list.append('m: convert to grayscale and convolute with gradient')
        list.append('p: convert to grayscale and plot the gradient vector with trackbar controling interval')
        list.append('r: convert to grayscale and rotate image with trackbar controling angle')
        list.append('h: display description')       
        for i in range(0, 17):
            cv2.putText(show, list[i], (0, (i+1)*interval), font, interval/55.0, color)
        cv2.imshow(winName, show)

    elif key == 27:
        cv2.destroyWindow(winName)
        break
