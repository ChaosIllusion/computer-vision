import cv2
import numpy as np
import sys
import math

filepath1 = "..//data//rock-l.tif"
filepath2 = "..//data//rock-r.tif"

def pointImage(filepath1, filepath2):
    global img1,img2,img1_copy,leftPoints,rightPoints
    leftPoints = []
    rightPoints = []
    img1 = readGrey(filepath1)
    img2 = readGrey(filepath2)
    cv2.namedWindow('image1')
    cv2.moveWindow("image1",50,100)  
    cv2.setMouseCallback('image1',drawPointLeft)
    cv2.namedWindow('image2')
    cv2.moveWindow("image2",600,100)  
    cv2.setMouseCallback('image2',drawPointRight)
    
    img1_copy = img1.copy()
    showInfo = False

    while(True):
        if not showInfo:
            cv2.imshow('image1',img1) 
        else:
            cv2.imshow('image1',img1_copy) 
        cv2.imshow('image2',img2)
        
        key = cv2.waitKey(20)
        if key == ord('i'):
            showInfo = False 
        elif key == ord('h'):
            height = img1.shape[0]
            interval = height/11
            font = cv2.FONT_HERSHEY_TRIPLEX
            color = (50, 150, 50)
            list = []
            list.append('cmd arguments:')
            list.append('--filename1: show the path of file of left image')
            list.append('--filename2: show the path of file of right image')
            list.append('Interface keys:')
            list.append('Left Hit: select point on image and store in sequence.')
            list.append('          The points with same index was correlated from two images.')
            list.append('ESC: finish selecting point')
            list.append('h: show help information')
            list.append('i: show origin image')
            img1_copy = img1.copy()
            for i in range(0, 9):
                cv2.putText(img1_copy, list[i], (0, (i+1)*interval), font, interval/120.0, color)
            showInfo = True
        elif key == 27:
            cv2.destroyAllWindows()
            break
    return leftPoints, rightPoints
    
def readGrey(filepath):
    img = cv2.imread(filepath, cv2.IMREAD_GRAYSCALE)
    GrayImg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    return GrayImg

def drawPointLeft(event, x, y, flags, param):
    if event==cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img1,(x,y),10,(255,0,0),2)
        cv2.circle(img1_copy,(x,y),10,(255,0,0),2)
        leftPoints.append([x,y])

def drawPointRight(event, x, y, flags, param):
    if event==cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img2,(x,y),10,(255,0,0),2)
        rightPoints.append([x,y])       

def normalizePoints(points):
    np_points = np.array(points)
    x_mean, y_mean = np.mean(np_points, axis=0)
    x_std, y_std = np.std(np_points, axis=0) 
    
    x_nor = (np_points[:,0] - x_mean)/x_std
    y_nor = (np_points[:,1] - y_mean)/y_std
    x_nor = x_nor.reshape(len(x_nor), 1)
    y_nor = y_nor.reshape(len(y_nor), 1)

    nor_points = np.hstack((x_nor, y_nor))
    M = np.array([[1/x_std, 0, -x_mean/x_std],[0, 1/y_std, -y_mean/y_std],[0,0,1]])
    return nor_points, M

def computingF(leftPoints, M_left, rightPoints, M_right):
    A = np.array([buildRow(leftPoints[i], rightPoints[i]) for i in range(len(leftPoints))])
    u, s, v = np.linalg.svd(A)
    F_rank3 = v[-1].reshape(3,3)
    uf, sf, vf = np.linalg.svd(F_rank3)
    sf[-1] = 0
    sf_mat = np.diag(sf)
    F_prime = np.dot(uf, np.dot(sf_mat, vf))
    return np.dot(M_right.T, np.dot(F_prime, M_left))
                                   
def buildRow(left_point, right_point):
    xl = left_point[0]
    yl = left_point[1]
    xr = right_point[0]
    yr = right_point[1] 
    return [xr*xl, xr*yl, xr, yr*xl, yr*yl, yr, xl, yl, 1]

def showEpipoleAndEpipolarLine(filepath1, filepath2, F):
    global img1,img2,img1_copy, F_mat
    F_mat = F
    img1 = readGrey(filepath1)
    img2 = readGrey(filepath2)
    el, er = computeEpipole(F)
    
    cv2.circle(img1,(int(el[0]), int(el[1])),5,(0,0,255),-1)
    cv2.circle(img2,(int(er[0]), int(er[1])),5,(0,0,255),-1)
    cv2.namedWindow('image1')
    cv2.moveWindow("image1",50,100)  
    cv2.setMouseCallback('image1',selectPointLeft)
    cv2.namedWindow('image2')
    cv2.moveWindow("image2",600,100)  
    cv2.setMouseCallback('image2',selectPointRight)
    
    img1_copy = img1.copy()
    showInfo = False
    
    print "show epipole on image."
    while(True):
        if not showInfo:
            cv2.imshow('image1',img1) 
        else:
            cv2.imshow('image1',img1_copy) 
        cv2.imshow('image2',img2)
        
        key = cv2.waitKey(20)
        if key == ord('i'):
            showInfo = False 
        elif key == ord('h'):
            height = img1.shape[0]
            interval = height/11
            font = cv2.FONT_HERSHEY_TRIPLEX
            color = (50, 150, 50)
            list = []
            list.append('Interface keys:')
            list.append('Left Hit: select point on one image ')
            list.append('          and show epipolar line on the other.')
            list.append('ESC: exit')
            list.append('h: show help information')
            list.append('i: show origin image')
            img1_copy = img1.copy()
            for i in range(0, len(list)):
                cv2.putText(img1_copy, list[i], (0, (i+1)*interval), font, interval/120.0, color)
            showInfo = True
        elif key == 27:
            cv2.destroyAllWindows()
            break
    

def computeEpipole(F):
    u, s, v = np.linalg.svd(F)
    er = u[:,-1]
    el = v[-1].reshape(3,1)
    return el/el[2], er/er[2]

def selectPointLeft(event, x, y, flags, param):
    if event==cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img1,(x,y),5,(255,0,0),2)
        cv2.circle(img1_copy,(x,y),5,(255,0,0),2)
        drawPipoleLine(x,y, img2)
        

def selectPointRight(event, x, y, flags, param):
    if event==cv2.EVENT_LBUTTONDOWN:
        cv2.circle(img2,(x,y),5,(255,0,0),2)
        drawPipoleLine(x,y, img1, False)
        drawPipoleLine(x,y, img1_copy, False)

def drawPipoleLine(x, y, img, rightSide = True):
    if rightSide == True:
        eq = F_mat.dot([x,y,1])
    else:
        pr = np.array([x,y,1])
        eq = pr.T.dot(F)

    y_max = img.shape[0]
    x_0Y = int(-eq[2]/eq[0])
    x_maxY = int(-(eq[2]+y_max*eq[1])/eq[0])
    cv2.line(img,(x_0Y, 0), (x_maxY, y_max),(0,255,0),2)



if len(sys.argv) == 3:
    filepath1 = sys.argv[1]
    filepath2 = sys.argv[2]
    print "Received new file path:"
    print "path1: %s" %(sys.argv[1])
    print "path2: %s" %(sys.argv[2])

print "Please select at least 8 pairs of points on both images in sequence"
pointImage(filepath1, filepath2)
minlen = min(len(leftPoints), len(rightPoints))
while minlen <8:
    print "You should select at least 8 points, now only %d pairs of points" %(minlen)
    print "please select again"
    pointImage(filepath1, filepath2)
    minlen = min(len(leftPoints), len(rightPoints))

print "computing fundemental matrix."
left_nor_P, M_left = normalizePoints(leftPoints)
right_nor_P, M_right = normalizePoints(rightPoints)
F = computingF(left_nor_P, M_left, right_nor_P, M_right)
print "The value of F is:"
print F

showEpipoleAndEpipolarLine(filepath1, filepath2, F)
print "program exit."