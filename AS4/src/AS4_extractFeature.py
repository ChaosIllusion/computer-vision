import cv2
import numpy as np
from operator import itemgetter, attrgetter
import math
import sys

filename = "..//data//Rubik-cube.JPG"
outfile = "..//data//cube-imagePt.txt"

def detectCorner(img):
	gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

	# Simply use cornerHarris to detect corner and following the instruction.
	gray = np.float32(gray)
	dst = cv2.cornerHarris(gray,2,3,0.04)
	dst = cv2.dilate(dst,None)
	ret, dst = cv2.threshold(dst,0.01*dst.max(),255,0)
	dst = np.uint8(dst)

	# find centroids
	ret, labels, stats, centroids = cv2.connectedComponentsWithStats(dst)

	# define the criteria to stop and refine the corners
	criteria = (cv2.TERM_CRITERIA_EPS + cv2.TERM_CRITERIA_MAX_ITER, 100, 0.001)
	corners = cv2.cornerSubPix(gray,np.float32(centroids),(7,7),(-1,-1),criteria)
	return corners

# load file
if len(sys.argv) >= 2:
    	print"User defined input file loading...\n"
        filename = sys.argv[1]
        i_path = sys.argv[2]

img = cv2.imread(filename)
img_copy = img.copy()
corners = detectCorner(img)

# Remove repeated corners.
c_int = np.int0(corners)
c_int = sorted(c_int, key=itemgetter(0,1))
c_fixed = []
c_fixed.append(c_int[0])
for i in range(1, len(corners)):
    if c_int[i][0] != c_int[i-1][0] or c_int[i][1] != c_int[i-1][1]:
        c_fixed.append(c_int[i])

# show and write corners
font = cv2.FONT_HERSHEY_TRIPLEX
color = (255, 150, 0)
index = 1
img_fd = open(outfile, 'w+')
print"Saveing corners coords into outfile: %s" %outfile
img_fd.writelines([str(len(c_fixed)), "\n"])
for i in xrange(len(c_fixed)):
    cv2.putText(img_copy, str(index),(c_fixed[i][0], c_fixed[i][1]), font, 0.5, color)
    cv2.rectangle(img_copy, (c_fixed[i][0] - 10, c_fixed[i][1] - 10), (c_fixed[i][0] + 10, c_fixed[i][1] + 10), [0,0,125])
    line = [str(c_fixed[i][0]), " ", str(c_fixed[i][1]), '\n']
    img_fd.writelines(line)
    index+=1

img_fd.close()
print"showing the image with detected corners"
cv2.imshow('dst',img_copy)
if cv2.waitKey(0) & 0xff == 27:
    cv2.destroyAllWindows()