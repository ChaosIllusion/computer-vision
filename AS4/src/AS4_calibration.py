import cv2
import numpy as np
import random
import math
import sys

worldP_path = "..//data//ncc-worldPt.txt"
imgP_path = "..//data//ncc-imagePt.txt"
RANSAC_path = "RANSAC.config"

def loadPoint(worldP_path, imgP_path):
    worldP = []
    world_fd = open(worldP_path)
    world_fd.readline()
    line = world_fd.readline()
    while line:
        tokens = line.split()
        worldP.append([float(tokens[0]), float(tokens[1]), float(tokens[2]), 1])
        line = world_fd.readline()
    
    imgP = []
    img_fd = open(imgP_path)
    img_fd.readline()
    line = img_fd.readline()
    while line:
        tokens = line.split()
        imgP.append([float(tokens[0]), float(tokens[1]), 1])
        line = img_fd.readline()
    return worldP, imgP

def loadRANSAC(RANSAC_path):
    RANSAC_para = []
    RANSAC_fd = open(RANSAC_path)
    line = RANSAC_fd.readline()
    while line:
        tokens = line.split('=')
        RANSAC_para.append(tokens[1])
        line = RANSAC_fd.readline()
    
    n = int(RANSAC_para[0])
    d = float(RANSAC_para[1])
    kmax = int(RANSAC_para[2])
    probability = float(RANSAC_para[3])
    w_init = float(RANSAC_para[4])
    
    return n, d, kmax, probability, w_init

def RANSAC(worldP, imgP, n, d, kmax, probability, w_init):
    inlierNum = 0
    M = np.zeros((3,4))
    
    d = int(d * len(worldP))
    i = 0
    k = math.log(1-probability) / math.log(1-math.pow(w_init, n))
    while i <= min(kmax, k):
        i+=1
        worldP_random, imgP_random = randomPick(worldP, imgP, n)
        newM = caculateM(worldP_random, imgP_random)
        worldP_inlier, imgP_inlier = findInlier(newM, worldP, imgP)
        
        w = float(len(worldP_inlier))/ len(worldP)
#         print"Iteration %d: the lnlier pencentage is %.1f%%" %(i, w*100)
        if(w == 1.0):
            return caculateM(worldP_inlier, imgP_inlier)
        k = math.log(1-probability)/math.log(1-math.pow(w, n))
        
        if(len(worldP_inlier) > d and len(worldP_inlier) > inlierNum):
            inlierNum = len(worldP_inlier)
            M = caculateM(worldP_inlier, imgP_inlier)

    return M

def randomPick(worldP, imgP, n):
    randomIndex = random.sample(range(0, len(imgP)), n)
    worldP_random = [worldP[i] for i in randomIndex]
    imgP_random = [imgP[i] for i in randomIndex]

    return worldP_random, imgP_random

def caculateM(worldP, imgP):
    row = 2*len(worldP)
    A = np.zeros((row, 12), dtype='float64')
    for i in xrange(len(worldP)):
        r1 = 2*i
        A[r1][0:4] = worldP[i]
        A[r1][4:8] = 0
        A[r1][8:12] = list(-np.dot(imgP[i][0], worldP[i]))
        r2 = 2*i+1
        A[r2][0:4] = 0
        A[r2][4:8] = worldP[i]
        A[r2][8:12] = list(-np.dot(imgP[i][1], worldP[i]))
    
    u, s, v = np.linalg.svd(A)
    M_hat = v[-1].reshape((3,4))
    return M_hat

def findInlier(M, worldP, imgP):
    distance = [computeDistance(M, worldP[i], imgP[i]) for i in xrange(len(worldP))]
    med = 1.5 * np.median(distance)
    
    index_inlier = [i for i in xrange(len(worldP)) if distance[i] < med]
    worldP_inlier = [worldP[i] for i in index_inlier]
    imgP_inlier = [imgP[i] for i in index_inlier]
    return worldP_inlier, imgP_inlier

def computeDistance(M, P, p):
    p_2dh = M.dot(P)
    p_ = p_2dh/p_2dh[2]
    return math.sqrt(math.pow(p_[0]-p[0],2) + math.pow(p_[1]-p[1],2))

def extractParameter(M, P, p):
    a1 = np.array(M[0][0:3]).T
    a2 = np.array(M[1][0:3]).T
    a3 = np.array(M[2][0:3]).T
    b = np.array(M[:,3])
    
    rho = 1/np.linalg.norm(a3)
    u0 = math.pow(rho,2) * np.dot(a1,a3)
    v0 = math.pow(rho,2) * np.dot(a2,a3)
    av = math.sqrt(math.pow(rho,2) * np.dot(a2,a2) - math.pow(v0,2))
    s = math.pow(rho,4) / av * np.dot(np.cross(a1,a3),np.cross(a2,a3))
    au = math.sqrt(math.pow(rho,2) * np.dot(a1,a1) - math.pow(s,2) - math.pow(u0,2))
    K = ([av,s,u0],[0,au,v0],[0,0,1])
    epsilon = np.sign(b[2])
    T = epsilon * rho * np.dot(np.linalg.inv(K),b)
    r3 = rho * epsilon * a3
    r1 = math.pow(rho,2) / av * np.cross(a2,a3)
    r2 = np.cross(r3,r1)
    R = np.stack((r1.T, r2.T, r3.T))
    
    error = [computeDistance(M, P[i], p[i]) for i in xrange(len(P))]
    
    print"Key parameters:"
    print"(u0,v0)         = (%.2f,%.2f)" %(u0, v0)
    print"(alphaU,alphaV) = (%.2f,%.2f)" %(au, av)
    print"s               = %.2f" %s
    print"T*              = (%.2f,%.2f,%.2f)" %(T[0], T[1], T[2])
    print"R*              = (%.6f,%.6f,%.6f)" %(R[0][0], R[0][1], R[0][2])
    print"                  (%.6f,%.6f,%.6f)" %(R[1][0], R[1][1], R[1][2])
    print"                  (%.6f,%.6f,%.6f)\n" %(R[2][0], R[2][1], R[2][2])
    print"error:" 
    print (error)

def main():
    if len(sys.argv) >= 3:
    	print"User defined input file loading...\n"
        w_path = sys.argv[1]
        i_path = sys.argv[2]
        if len(sys.argv) >= 4:
        	R_path = sys.argv[3]
        else:
        	R_path = RANSAC_path
    else:
        w_path = worldP_path
        i_path = imgP_path
        R_path = RANSAC_path

    worldP, imgP = loadPoint(w_path, i_path)
    n, d, kmax, probability, w_init = loadRANSAC(R_path)
    M = RANSAC(worldP, imgP, n, d, kmax, probability, w_init)
    extractParameter(M, worldP, imgP)

if __name__ == '__main__':
    main()
